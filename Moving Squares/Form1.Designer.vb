﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class Form1
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container()
        Me.ship = New System.Windows.Forms.PictureBox()
        Me.Timer1 = New System.Windows.Forms.Timer(Me.components)
        Me.tmrMove = New System.Windows.Forms.Timer(Me.components)
        Me.tmrAlien = New System.Windows.Forms.Timer(Me.components)
        Me.lblDebug = New System.Windows.Forms.Label()
        Me.tmrAlienShoot = New System.Windows.Forms.Timer(Me.components)
        CType(Me.ship, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'ship
        '
        Me.ship.BackColor = System.Drawing.Color.Red
        Me.ship.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.ship.Location = New System.Drawing.Point(174, 231)
        Me.ship.Name = "ship"
        Me.ship.Size = New System.Drawing.Size(30, 30)
        Me.ship.TabIndex = 0
        Me.ship.TabStop = False
        '
        'Timer1
        '
        '
        'tmrMove
        '
        Me.tmrMove.Enabled = True
        '
        'tmrAlien
        '
        Me.tmrAlien.Enabled = True
        '
        'lblDebug
        '
        Me.lblDebug.AutoSize = True
        Me.lblDebug.Location = New System.Drawing.Point(317, 248)
        Me.lblDebug.Name = "lblDebug"
        Me.lblDebug.Size = New System.Drawing.Size(0, 13)
        Me.lblDebug.TabIndex = 1
        '
        'tmrAlienShoot
        '
        Me.tmrAlienShoot.Enabled = True
        Me.tmrAlienShoot.Interval = 1000
        '
        'Form1
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.BackColor = System.Drawing.Color.Black
        Me.ClientSize = New System.Drawing.Size(368, 273)
        Me.Controls.Add(Me.lblDebug)
        Me.Controls.Add(Me.ship)
        Me.ForeColor = System.Drawing.Color.White
        Me.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle
        Me.KeyPreview = True
        Me.MaximizeBox = False
        Me.MinimizeBox = False
        Me.Name = "Form1"
        Me.ShowIcon = False
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen
        Me.Text = "Space..."
        CType(Me.ship, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub
    Friend WithEvents ship As System.Windows.Forms.PictureBox
    Friend WithEvents Timer1 As System.Windows.Forms.Timer
    Friend WithEvents tmrMove As System.Windows.Forms.Timer
    Friend WithEvents tmrAlien As System.Windows.Forms.Timer
    Friend WithEvents lblDebug As System.Windows.Forms.Label
    Friend WithEvents tmrAlienShoot As System.Windows.Forms.Timer

End Class
