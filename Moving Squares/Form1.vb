﻿Public Class Form1

    Dim arrBullets(100) As PictureBox       ' array used to store bullets
    Dim arrAliens(39) As PictureBox         ' array used to store the alien fleet
    Dim arrStars(99) As PictureBox          ' array used to store the heavens
    Dim arrAlienBullets(99) As PictureBox   ' array used to store death from above

    Private Sub Form1_KeyPress(sender As Object, e As KeyPressEventArgs) Handles Me.KeyPress

        ' Here we use a CASEWHERE control structure to respond to
        ' user KeyPress events. Taking the input of the user, we
        ' move the PictureBox 'ship' in the direction the user
        ' indicates by using the PictureBox's .top and .left 
        ' properties

        Select Case e.KeyChar
            Case "a"
                ship.Left -= 10
            Case "d"
                ship.Left += 10
            Case "w"
                ship.Top -= 10
            Case "s"
                ship.Top += 10
            Case " "
                shootstuff()
        End Select

    End Sub

    Private Sub createBullets()

        ' This is a utility sub that creates the bullets we will
        ' shoot at the aliens at runtime.

        For i = 0 To arrBullets.Length - 1

            Dim bullet As New PictureBox            ' create a new instance of a pBox
            bullet.Size = New Size(2, 2)            ' create a new size and apply it to the pBox
            bullet.Location = New Point(10, 10)     ' create a new point on the form and set the pBox to it
            bullet.BackColor = Color.GreenYellow       ' choose a nice colour
            bullet.Visible = False                  ' make it invisible

            arrBullets(i) = bullet                  ' add the pBox to the array of bullets
            Me.Controls.Add(bullet)                 ' add the pBox to the Form.

        Next

    End Sub

    Private Sub createAlienBullets()

        ' This is a utility sub that creates the bullets we will
        ' shoot at the aliens at runtime.

        For i = 0 To arrBullets.Length - 2

            Dim bullet As New PictureBox            ' create a new instance of a pBox
            bullet.Size = New Size(5, 5)            ' create a new size and apply it to the pBox
            bullet.Location = New Point(10, 10)     ' create a new point on the form and set the pBox to it
            bullet.BackColor = Color.LightBlue     ' choose a nice colour
            bullet.Visible = False                  ' make it invisible

            arrAlienBullets(i) = bullet                  ' add the pBox to the array of bullets
            Me.Controls.Add(bullet)                 ' add the pBox to the Form.

        Next

    End Sub

    Private Sub createAlienFleet()

        ' This sub-routine creates a fleet of aliens in 4 rows of 10 columns.
        ' It sets all the properties (attributes) of the aliens and then
        ' adds them to an array that we can access later.

        Dim i As Integer

        For x = 0 To 3
            For y = 0 To 9

                Dim alien As New PictureBox
                alien.Height = 20
                alien.Width = 20
                alien.Top = 10 + x * 22
                alien.Left = 10 + y * 22
                alien.BackColor = Color.Green
                alien.BorderStyle = BorderStyle.FixedSingle
                alien.Visible = True
                arrAliens(i) = alien
                i += 1
                Me.Controls.Add(alien)

            Next
        Next

    End Sub

    Private Sub shootstuff()

        ' In this sub-routine we look for an available bullet to shoot.
        ' This is defined as being a bullet that is not visible. We 
        ' shoot it by making it visible. This property is picked up
        ' by the Timer1 tick event and the bullet is moved.

        For i = 0 To arrBullets.Length - 1
            If arrBullets(i).Visible = False Then
                arrBullets(i).Top = ship.Top
                arrBullets(i).Left = ship.Left + (ship.Width / 2)
                arrBullets(i).Visible = True
                'My.Computer.Audio.Play(Application.StartupPath & "\sounds\shoot.wav", AudioPlayMode.Background)
                Exit For
            End If
        Next

    End Sub

    Private Sub Form1_Load(sender As Object, e As EventArgs) Handles MyBase.Load

        Timer1.Enabled = True                       ' enable the timer
        createBullets()                             ' create the bullets
        createAlienFleet()                          ' create the alien fleet
        createTheStars()                            ' create space
        createAlienBullets()                        ' create alien bullets

    End Sub

    Private Sub Timer1_Tick(sender As Object, e As EventArgs) Handles Timer1.Tick

        For i = 0 To arrBullets.Length - 1          ' for each bullet
            If arrBullets(i).Visible = True Then    ' check if bullet is available
                arrBullets(i).Top -= 10             ' move bullet up 10
                If arrBullets(i).Top <= 0 Then      ' check if past top of form
                    arrBullets(i).Visible = False   ' and then make invisible if necessary
                End If
            End If
        Next

        For i = 0 To arrAlienBullets.Length - 1          ' for each bullet
            If arrAlienBullets(i).Visible = True Then    ' check if bullet is available
                arrAlienBullets(i).Top += 10             ' move bullet up 10
                If arrAlienBullets(i).Top >= Me.Height Then      ' check if past top of form
                    arrAlienBullets(i).Visible = False   ' and then make invisible if necessary
                End If
            End If
        Next

        checkOurHits()                              ' call the sub-routine to defeat enemies
        killAnimation()
        moveStars()
        checkTheirHits()

    End Sub

    Private Sub checkOurHits()

        ' Here we check each bullet against each alien to 
        ' see if we have made a hit. We are using a FOR EACH
        ' statement to go through our arrays. This allows us
        ' to test each item in the array without having to 
        ' know how many items are in the array first.

        For Each alien As PictureBox In arrAliens                   ' for each alien in our array of aliens
            For Each b As PictureBox In arrBullets                  ' for each bullet
                If b.Visible = True And alien.Visible = True Then   ' if bullet & alien are visible
                    If b.Bounds.IntersectsWith(alien.Bounds) Then   ' check for intersection
                        alien.Tag = "dead"                          ' set a flag to remove the alien
                        b.Visible = False                           ' "kill" bullet
                    End If
                End If
            Next
        Next

    End Sub

    Private Sub checkTheirHits()

        For Each bullet As PictureBox In arrAlienBullets
            If bullet.Visible = True Then
                If bullet.Bounds.IntersectsWith(ship.Bounds) Then
                    bullet.Visible = False
                    ship.Visible = False
                    MsgBox("You lose, loser")
                End If
            End If
        Next

    End Sub

    Private Sub killAnimation()

        ' We animate the "death" of the aliens by shrinking 
        ' them to nothing. To keep them centred, we shrink
        ' by 2 and move left and down by 1. Once they are 
        ' below 1px by 1px, we make them invisible, remove
        ' the tag that set the pBox to be destroyed and
        ' move on.

        For Each alien As PictureBox In arrAliens
            If alien.Tag = "dead" Then
                Dim x = alien.Width
                alien.Size = New Size(x - 2, x - 2)
                alien.Left += 1
                alien.Top += 1
                If alien.Width <= 0 Then
                    alien.Visible = False
                    alien.Tag = ""
                End If
            End If
        Next
    End Sub

    Private Function getR(max As Integer)
        Dim x As Integer
        x = Int(Rnd() * max) + 1
        Return x
    End Function

    Private Sub createTheStars()

        For i = 0 To 99

            Randomize()
            Dim y As Integer = getR(Me.Height)
            Dim x As Integer = getR(Me.Width)
            Dim c As Integer = getR(200)
            Dim s As Integer = getR(3)

            Dim star As New PictureBox
            star.Location = New Point(x, y)
            star.Size = New Size(s, s)
            star.SendToBack()
            star.BackColor = Color.FromArgb(c, c, c)
            star.Tag = Int(c / 20)
            arrStars(i) = star
            Me.Controls.Add(star)

        Next

    End Sub

    Private Sub moveStars()

        For Each star As PictureBox In arrStars
            star.Top += Int(star.Tag)
            If star.Top > Me.Height Then star.Top = 0
        Next

    End Sub

    Private Sub tmrAlien_Tick(sender As Object, e As EventArgs) Handles tmrAlien.Tick

        Static dir As String = "right"

        For Each alien As PictureBox In arrAliens

            If dir = "right" Then
                alien.Left += 5
            Else
                alien.Left -= 5
            End If

        Next

        For Each alien As PictureBox In arrAliens

            If alien.Right >= Me.Width - 10 And alien.Visible = True Then
                dir = "left"
            ElseIf alien.Left <= 10 And alien.Visible = True Then
                dir = "right"
            End If

            'lblDebug.Text = dir
        Next



    End Sub

    Private Sub tmrAlienShoot_Tick(sender As Object, e As EventArgs) Handles tmrAlienShoot.Tick

        Dim x = Int(Rnd() * (arrAliens.Length - 1))

        For i = 0 To arrAlienBullets.Length - 1
            If arrAlienBullets(i).Visible = False Then
                arrAlienBullets(i).Left = arrAliens(x).Left
                arrAlienBullets(i).Top = arrAliens(x).Bottom
                arrAlienBullets(i).Visible = True
                Exit For
            End If
        Next

    End Sub
End Class
